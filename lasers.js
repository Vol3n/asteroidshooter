function Laser(spos,angle) {
  this.pos = createVector(spos.x,spos.y);
  this.vel = p5.Vector.fromAngle(angle);
  this.vel.mult(8);

  this.update = function () {
    this.pos.add(this.vel);
  }

  this.render = function () {
    push();
    stroke(244,255,0);
    strokeWeight(3);
    point(this.pos.x,this.pos.y);
    pop();
  }

  this.hits = function (asteroid) {
    if ( dist(this.pos.x,this.pos.y,asteroid.pos.x,asteroid.pos.y) < asteroid.r ) {
      return true;
    } else {
      return false;
    }
  }

  this.offscreen = function () {
    if ( this.pos.x > width || this.pos.x < 0 || this.pos.y > height || this.pos.y < 0){
      return true;
    } else {
      return false;
    }
  }
}
