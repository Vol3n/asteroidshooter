function Ship() {
    this.pos = createVector(width / 2, height / 2);
    this.r = 20;
    this.heading = 0;
    this.rotation = 0;
    this.vel = createVector(0,0);
    this.isBoosting = false;
    this.hp = 1;
    this.immune = false;

    this.boosting = function (b) {
      this.isBoosting = b;
    }

    this.edges = function () {
      if (this.pos.x > width + this.r) {
        this.pos.x = -this.r;
      } else if (this.pos.x < -this.r) {
        this.pos.x = width + this.r;
      } else if (this.pos.y > height + this.r) {
        this.pos.y = -this.r;
      } else if (this.pos.y < -this.r){
        this.pos.y = height + this.r;
      }
    }

    this.update = function () {
      if (this.isBoosting) {
        this.boost();
      }
      this.pos.add(this.vel);
      this.vel.mult(0.98);
    }

    this.boost = function () {
      var force = p5.Vector.fromAngle(this.heading);
      force.mult(0.4);
      this.vel.add(force);
    }

    this.render = function() {
        push();
        translate(this.pos.x, this.pos.y);
        rotate(this.heading + PI/2);
        fill(0);
        if (this.immune) {
          stroke(255);
        } else {
          noStroke();
        }

        if (this.hp >= 1) {
          triangle(-this.r, this.r, this.r, this.r, 0, -this.r);
          if (this.isBoosting) {
            fill(0,0,255,120);
            noStroke();
            triangle(0,2.5*this.r, 0.5*this.r, this.r, -0.5*this.r,this.r)
          }
          if(this.hp >= 2){
            fill(155,155,255, this.hp*20);
            noStroke();
            ellipse(0,5,this.r*3.5,this.r*3.5);
          }
        }
        pop();
    }

    this.turn = function() {
        this.heading += this.rotation;
    }

    this.setRotation = function (a) {
      this.rotation = a;
    }

    this.followMouse = function () {
      var v1 = createVector(width);
      if (this.heading > TWO_PI) {
        this.heading = 0;
      }
      this.heading = (p5.Vector.angleBetween(v1,v2) - PI/2);
      // console.log(p5.Vector.angleBetween(this.pos, mousePos));
      console.log(this.heading.toFixed(2) + " " + (p5.Vector.angleBetween(screenCenter, mousePos)));
    }

    this.isDestroyed = function () {
      if (this.hp == 0) {
        return true;
      } else {
        return false;
      }
    }
}
