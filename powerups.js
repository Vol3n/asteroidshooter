function Powerups(type){
  this.pos = createVector(random(width),random(height));
  this.type = type;
  this.r = 40;
  this.lifespan = 500;
  this.used = false;

  this.render = function () {
    switch (type) {
      case 1:
      push();
      noStroke();
      fill(155,155,255,100);
      ellipse(this.pos.x,this.pos.y,this.r,this.r);
      pop();
      break;
    }
  }

  this.update = function () {
    this.lifespan--;
  }

  this.pickup = function (ship) {
    if(dist(this.pos.x,this.pos.y,ship.pos.x,ship.pos.y) < this.r + ship.r){
      if (!this.used) {
        ship.hp++;
        this.used = true;
        this.r = 0;
        return true;
      }
    } else {
      return false;
    }
  }
}
