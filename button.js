function Button(index,string){
  this.pos = new p5.Vector();
  this.text = string;
  this.height = windowHeight/100*5;
  this.width =  windowWidth/100*20;
  this.txtSize = (windowHeight/100*5)/2;
  this.offset = (index+1)*50+(index*this.height);
  this.pos.x = (windowWidth - this.width)/2;
  this.pos.y =  this.offset;

  this.render = function () {
    push();
    stroke(255);
    textAlign(CENTER,CENTER);
    textSize(this.txtSize);
    rect(this.pos.x, this.pos.y , this.width, this.height);
    text(this.text,windowWidth/2,this.offset+this.txtSize);
    pop();
  }
}
