

var Asteroid = function (pos,r) {
  if (pos) {
    this.pos = pos.copy();
  } else {
    this.pos = createVector(random(width), random(height));
  }
  if (r) {
    this.r = r;
  } else {
    this.r = floor(random(20,75));
  }

  this.total = floor(random(10,20));
  this.vel = p5.Vector.random2D();
  this.vel.mult(random(0.3,2.5));
  this.offset = [];
  for (var i = 0; i < this.total ; i++) {
    this.offset[i] = random(-10,10);
  }
  this.render = function () {
    push();
    stroke(255);
    noFill();
    translate(this.pos.x, this.pos.y);
    beginShape();
    for ( var i = 0 ; i < this.total ; i++) {
      var angle = map(i,0,this.total,0,TWO_PI);
      var x = (this.r + this.offset[i])*cos(angle);
      var y = (this.r + this.offset[i])*sin(angle);
      vertex(x,y);
    }
    endShape(CLOSE);
    pop();
  }

  this.update = function () {
    this.pos.add(this.vel);
  }

  this.edges = function () {
    if (this.pos.x > width + this.r) {
      this.pos.x = -this.r;
    } else if (this.pos.x < -this.r) {
      this.pos.x = width + this.r;
    } else if (this.pos.y > height + this.r) {
      this.pos.y = -this.r;
    } else if (this.pos.y < -this.r){
      this.pos.y = height + this.r;
    }
  }

  this.destroyed = function() {
    var newAsteroids = [];
    if(this.r > 30){
      newAsteroids.push(new Asteroid(this.pos, this.r/2));
      newAsteroids.push(new Asteroid(this.pos, this.r/2));
    }
    return newAsteroids;
  }

  this.hits = function(ship) {
    if(dist(this.pos.x,this.pos.y,ship.pos.x,ship.pos.y) < this.r + ship.r){
      return true;
    } else {
      return false;
    }
  }
}
