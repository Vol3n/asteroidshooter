function Explosion(pos,r) {
  this.pos = [];
  this.stroke = r*10;
  this.vel = [];
  this.particleCount = 50;

  for (var i = 0; i < this.particleCount; i++) {
    this.pos[i] = pos.copy();
  }

  for (var i = 0; i < this.particleCount; i++) {
    var angle = map(i,0,this.particleCount,0,TWO_PI);
    this.vel[i] =  p5.Vector.fromAngle(angle);
  }

  this.render = function () {
    push();
    stroke(this.stroke);
    strokeWeight(1);
    for (var i = 0; i < this.particleCount; i++) {
      point(this.pos[i].x,this.pos[i].y);
    }
    pop();
  }

  this.update = function () {
    for (var i = 0; i < this.particleCount; i++) {
      this.pos[i].add(this.vel[i]);
      if (this.stroke > 51) {
        this.stroke -= 0.05;
      }
    }
  }
}
