var ship;
var asteroids = [];
var lasers = [];
var explosions = [];
var powerups = [];
var explosionPos, explosionSize;
var gun;
var gunPower = 0;
var attackSpeed = 250;
var lastAttack = new Date().getTime();
var buttons = [];
var gameIsRunning = false;
var spawner;
var spawnerInterval = Math.random(10000,15000);
var score = 0;
var stars = [];
var maxAsteroids = 10;

var p;


function setup() {
    createCanvas(windowWidth, windowHeight);
    Highscores();
    frameRate(60);
    ship = new Ship();
    buttons.push(new Button(0,"New Game"));
    buttons.push(new Button(1,"High Scores"));
    buttons.push(new Button(2,"Quit"));

    for (var i = 0; i < 50; i++) {
      stars.push(new Star());
    }
}

function draw() {
    background(50);
    textSize(30);
    text("SCORE: " + score,150,50);
    text("HP: " + ship.hp, 150,100);
    push();
    translate(width / 2, height / 2);
    for (var i = 0; i < stars.length; i++) {
       stars[i].update();
       stars[i].show();
     }
     pop();
    asteroids.forEach(function(asteroid) {
      asteroid.render();
      if (gameIsRunning) {
        asteroid.update();
        if(asteroid.hits(ship)){
          if (!ship.immune) {
            ship.hp--;
            ship.immune = true;
            setInterval(function () {
              ship.immune = false;
              clearInterval();
            },3000);
          }
        }
      }
      asteroid.edges();
    });

    while (asteroids.length < maxAsteroids) {
      asteroids.push(new Asteroid());
    }


    powerups.forEach(function (powerup) {
      powerup.render();
      powerup.update();
      powerup.pickup(ship);
      if (!gameIsRunning) {
        powerup.lifespan++;
      }
      if (powerup.lifespan == 0) {
        powerups.splice(powerups.indexOf(powerup),1);
      }
    });

    explosions.forEach(function (explosion) {
      explosion.render();
      explosion.update();
      if (floor(explosion.stroke) == 50) {
        explosions.splice(explosions.indexOf(explosion),1);
      }
    });

    try {
      lasers.forEach(function (laser) {
        laser.render();
        laser.update();
        asteroids.forEach(function (asteroid) {
          if (laser.hits(asteroid)) {
            maxAsteroids = map(score,0,500,10,50);
            lasers.splice(lasers.indexOf(laser),1);
            asteroids.concat(asteroids,asteroid.destroyed());
            var remains = asteroid.destroyed();
            remains.forEach(function (remain) {
              asteroids.push(remain);
            })
            asteroids.splice(asteroids.indexOf(asteroid),1);
            explosionPos = asteroid.pos.copy();
            explosionSize = asteroid.r;
            score+=floor(100*(1/asteroid.r));
            throw "Hit";
          }
          if (laser.offscreen()) {
            lasers.splice(lasers.indexOf(laser),1);
            throw "BreakException";
          }
        })
      });
    } catch (e) {
      switch (e) {
        case "Hit":
          explosions.push(new Explosion(explosionPos,explosionSize));
          break;
        default:
      }
    } finally {

    }

    ship.render();
    if (gameIsRunning) {
      ship.turn();
      ship.update();
      if (ship.isDestroyed()) {
        gameIsRunning = false;
      }
      ship.edges();
    }


    buttons.forEach(function (button) {
      button.render();
    });
}

function keyPressed(){
  if (gameIsRunning) {
    if (keyCode == RIGHT_ARROW) {
      ship.setRotation(0.1);
    } else if (keyCode == LEFT_ARROW) {
      ship.setRotation(-0.1)
    } else if (keyCode == UP_ARROW){
      ship.boosting(true);
    } else if (key == ' ') {
      var curAttack  = new Date().getTime();
      if (curAttack - lastAttack > attackSpeed){
        switch (gunPower) {
          case 0:
            lasers.push(new Laser(ship.pos, ship.heading));
            break;
          case 1:
            lasers.push(new Laser(ship.pos, ship.heading));
            lasers.push(new Laser(ship.pos, ship.heading-PI/6));
            lasers.push(new Laser(ship.pos, ship.heading+PI/6));

        }
        lastAttack = curAttack;
      }
      gun = setInterval(function () {
        switch (gunPower) {
          case 0:
            lasers.push(new Laser(ship.pos, ship.heading));
            break;
          case 1:
            lasers.push(new Laser(ship.pos, ship.heading));
            lasers.push(new Laser(ship.pos, ship.heading-PI/6));
            lasers.push(new Laser(ship.pos, ship.heading+PI/6));

        }
      },attackSpeed);
    }
  }


  if (keyCode == 27) {
    if (gameIsRunning) {
      gameIsRunning = !gameIsRunning;
      buttons.push(new Button(0,"Resume Game"));
      buttons.push(new Button(1,"New Game"));
      buttons.push(new Button(2,"High Scores"));
      buttons.push(new Button(3,"Quit"));
    } else {
      buttons = [];
      gameIsRunning = !gameIsRunning;
      spawnPowerups();
    }
  }
}

function mousePressed() {
  buttons.forEach(function (button) {
    if( (mouseX >= button.pos.x && mouseX <= button.pos.x + button.width) && (mouseY >= button.pos.y && mouseY <= button.pos.y + button.height) ){
      if(button.text == "New Game"){
        buttons = [];
        ship = new Ship();
        // setTimeout(spawnPowerups,5000);
        spawnPowerups();
        gameIsRunning = true;
      } else if ( button.text == "Resume Game"){
        buttons = [];
        gameIsRunning = true;
      } else if (button.text == "Quit"){
        window.close();
      }
    }
  })
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}

function keyReleased() {
  if (keyCode == RIGHT_ARROW) {
    ship.setRotation(0);
  } else if (keyCode == LEFT_ARROW) {
    ship.setRotation(0)
  } else if (keyCode == UP_ARROW){
    ship.boosting(false);
  } else if (key == ' ') {
    clearInterval(gun);
  } else if (key == 1) {
    gunPower = 0;
  } else if (key == 2) {
    gunPower = 1;
  }
}

function spawnPowerups() {
  clearInterval(spawner);
  spawner = setInterval(function () {
    powerups.push(new Powerups(1));
    spawnerInterval = random(10000,15000);
    if (gameIsRunning) {
      spawnPowerups();
    }
  }, spawnerInterval);
}
